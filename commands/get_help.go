package commands

// GetHelpMessage returns the details about the available commands
func GetHelpMessage() string {
	return "```" + `
/qod            - get quote of the day
/qod <category> - get quote of the day for a specific category
/qod categories - get available categories
/qod help       - show this help message
` + "```"
}
