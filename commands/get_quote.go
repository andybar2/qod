package commands

import (
	"encoding/json"
	"errors"
	"net/http"
)

// GetQuotesResult wraps the contents of the quotes
type GetQuotesResult struct {
	Contents *QuotesContents `json:"contents"`
}

// QuotesContents has an array of quotes
type QuotesContents struct {
	Quotes []*Quote `json:"quotes"`
}

// Quote represents a quote
type Quote struct {
	Quote  string `json:"quote"`
	Author string `json:"author"`
}

// GetQuote gets the quote of the day by calling the API from https://theysaidso.com
func GetQuote(category string) (*Quote, error) {
	httpClient := &http.Client{}
	url := "http://quotes.rest/qod.json"

	if category != "" {
		url += "?category=" + category
	}

	resp, err := httpClient.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	result := &GetQuotesResult{}

	if err = json.NewDecoder(resp.Body).Decode(result); err != nil {
		return nil, err
	}

	if result.Contents == nil || len(result.Contents.Quotes) == 0 {
		return nil, errors.New("No quotes found")
	}

	return result.Contents.Quotes[0], nil
}
