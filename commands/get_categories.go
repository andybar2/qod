package commands

import (
	"encoding/json"
	"errors"
	"net/http"
	"sort"
)

// GetCategoriesResult wraps the contents of the categories
type GetCategoriesResult struct {
	Contents *CategoriesContents `json:"contents"`
}

// CategoriesContents has a map of categories
type CategoriesContents struct {
	Categories map[string]string `json:"categories"`
}

// GetCategories gets the available categories for quotes by calling the API from https://theysaidso.com
func GetCategories() ([]string, error) {
	httpClient := &http.Client{}
	url := "http://quotes.rest/qod/categories.json"

	resp, err := httpClient.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	result := &GetCategoriesResult{}

	if err = json.NewDecoder(resp.Body).Decode(result); err != nil {
		return nil, err
	}

	if result.Contents == nil {
		return nil, errors.New("No categories found")
	}

	categories := []string{}

	for cat := range result.Contents.Categories {
		categories = append(categories, cat)
	}

	sort.Strings(categories)

	return categories, nil
}
