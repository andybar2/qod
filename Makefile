# Ignore `No rule to make target` errors
%:
	@echo ""

# Install development tools
tools:
	@go get -u github.com/codegangsta/gin
.PHONY: tools

# Run development environment
dev:
	@up start
.PHONY: dev

# Deploy production environment
prod:
	@read -p "Do you really want to deploy to production? (y/n) " RESP; \
	if [ "$$RESP" = "y" ]; then \
		up deploy production; \
	fi
.PHONY: prod
