package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"andybar2/qod/commands"
)

func main() {
	addr := ":" + os.Getenv("PORT")
	http.HandleFunc("/", handle)
	log.Fatal(http.ListenAndServe(addr, nil))
}

func handle(w http.ResponseWriter, r *http.Request) {
	// parse form body and get command from the "text" parameter inside it
	if err := r.ParseForm(); err != nil {
		log.Printf("Error parsing form: %s", err)
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	cmd := r.Form.Get("text")

	// execute command
	switch cmd {
	case "help":
		// get help message
		msg := commands.GetHelpMessage()

		// write response
		fmt.Fprint(w, msg)

	case "categories":
		// get categories
		categories, err := commands.GetCategories()
		if err != nil {
			log.Printf("Error getting categories: %s", err)
			fmt.Fprint(w, "Sorry, something went wrong. Try again in a few mins please.\n")
			return
		}

		// write response
		fmt.Fprintf(w, "Available categories: %s\n", strings.Join(categories, ", "))

	default:
		// get quote (cmd = quote category or empty)
		quote, err := commands.GetQuote(cmd)
		if err != nil {
			log.Printf("Error getting quote: %s", err)
			fmt.Fprint(w, "Sorry, something went wrong. Try again in a few mins please.\n")
			return
		}

		// write response
		w.Header().Set("Content-type", "application/json")
		fmt.Fprintf(w, `{"response_type": "in_channel", "text": "_\"%s\"_ by %s"}`, quote.Quote, quote.Author)
	}
}
